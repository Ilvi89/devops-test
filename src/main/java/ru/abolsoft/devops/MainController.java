package ru.abolsoft.devops;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
public class MainController {

    private final DataRepository dataRepository;

    public MainController(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    @GetMapping
    public ResponseEntity<?> hi() {

        List<Data> data = dataRepository.findAll();
        return ResponseEntity.of(Optional.of(data));
    }

    @PostMapping()
    public ResponseEntity<?> pingPong() {
        var data = dataRepository.save(new Data(LocalDateTime.now()));
        return ResponseEntity.of(Optional.of(data));
    }
}



