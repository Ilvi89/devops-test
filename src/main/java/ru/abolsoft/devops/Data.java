package ru.abolsoft.devops;

import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
public class Data {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;


    private LocalDateTime date;

    public Data(LocalDateTime date) {
        this.date = date;
    }

    public Data() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
